import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  uri = 'http://localhost:8001';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  getTasks (offSet, name = "") {
    return this.http.get(`${this.uri}/tasks/${offSet}/${name}`);
  }

  getTaskById(id) {
    return this.http.get(`${this.uri}/task/${id}`);
  }

  getTotalTasks(name) {
    return this.http.get(`${this.uri}/totalTasks/${name}`);
  }

  addTask(name) {
    const task = {
      name: name
    };

    return this.http.post(`${this.uri}/task/`, task);
  }

  updateTask(id, name) {
    const task = {
      name: name
    };

    return this.http.put(`${this.uri}/task/${id}`, task);
  }

  deleteTask(id) {
    return this.http.delete(`${this.uri}/task/${id}`);
  }
}
