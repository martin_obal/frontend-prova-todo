import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './components/tasks/list/list.component';
import { CreateComponent } from './components/tasks/create/create.component';
import { EditComponent } from './components/tasks/edit/edit.component';

const routes: Routes = [
  { path: 'edit/:id', component: EditComponent },
  { path: 'create', component: CreateComponent },
  { path: 'tasks', component: ListComponent },
  { path: '', redirectTo: 'tasks', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
