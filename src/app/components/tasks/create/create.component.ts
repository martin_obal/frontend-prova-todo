import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { TasksService } from '../../../services/tasks/tasks.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  createForm: FormGroup;

  constructor(private tasksService: TasksService, private fb: FormBuilder, private router: Router, private snackBar: MatSnackBar) {
    this.createForm = this.fb.group({
      name: ['', Validators.required],
    });
  }

  addTask(name) {
    console.log('teste');
    this.tasksService.addTask(name).subscribe(() => {
      this.snackBar.open('Task criada com sucesso!', 'OK', {
        duration: 3000
      });
      this.router.navigate(['/tasks']);
    }, () => {
      this.snackBar.open('Ocorreu um erro ao criar a task!', 'OK', {
        duration: 3000
      });
    });
  };

  ngOnInit() {
  }

}
