import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../../services/tasks/tasks.service';
import { Task } from '../../../models/tasks/tasks.model';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  tasks: Task[];
  displayedColumns = ['name', 'actions'];

  totalPages;
  currentPage = 1;
  arrayTotalPages = [];
  limitPages = 10;
  offSet = 0;

  constructor(private tasksService: TasksService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getTasks();
    this.countTotalTasks();
  }

  getTasks(name = "") {
    this.tasksService
      .getTasks(this.offSet, name)
      .subscribe((data: Task[]) => {

        this.tasks = data;
      }, () => {
        this.snackBar.open('Ocorreu um erro ao carregar as tasks!', 'OK', {
          duration: 3000
        });
      });
  }

  editTask(id) {
    this.router.navigate([`/edit/${id}`]);
  }

  deleteTask(id) {
    this.tasksService
      .deleteTask(id)
      .subscribe(() => {
        this.getTasks();
        this.snackBar.open('Task excluída com sucesso!', 'OK', {
          duration: 3000
        });
    }, () => {
      this.snackBar.open('Ocorreu um erro ao excluir a task!', 'OK', {
        duration: 3000
      });
    });
  }

  countTotalTasks(name = "") {
    this.tasksService
    .getTotalTasks(name)
    .subscribe((total: number) => {
      let mod = (total % this.limitPages);

      if (mod == total) {
        mod = 1;
      }

      this.totalPages = mod;
      this.createPages();
    });
  }

  createPages() {
    this.arrayTotalPages = [];
    for (let i = 1; i <= this.totalPages; i++) {
      this.arrayTotalPages.push(i);
    }
  }

  goToPage(number) {
    this.currentPage = number;
    this.offSet = ((this.currentPage - 1) * this.limitPages);
    this.getTasks();
  }

  goToLastPage() {
    console.log(this.totalPages);
    this.goToPage(this.totalPages);
  }

  setSelected(elm) {
    let actives = document.getElementsByClassName("active");

    if (actives.length > 0) {
      actives[0].classList.remove('active');
    }

    elm.classList.add('active');
  }

  onKeyUp(input) {
    this.getTasks(input.value);
    this.countTotalTasks(input.value);
  }
}
