import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { TasksService } from '../../../services/tasks/tasks.service';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: String;
  task: any = {};
  editForm: FormGroup;

  constructor(private tasksService: TasksService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar) {
    this.createForm();
  }

  createForm() {
    this.editForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.tasksService.getTaskById(this.id).subscribe(res => {
        this.task = res;
        this.editForm.get('name').setValue(this.task.name);
      })
    });
  }

  updateTask(name) {
    this.tasksService.updateTask(this.id, name).subscribe(() => {
      this.snackBar.open('Task atualizada com sucesso!', 'OK', {
        duration: 3000
      });
    }, () => {
      this.snackBar.open('Ocorreu um erro ao atualizar a task!', 'OK', {
        duration: 3000
      });
    });
  }
}
